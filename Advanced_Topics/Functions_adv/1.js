// CLOSURES

const first = () => {
  const greet = "Hi";
  const second = () => {
    alert(greet);
  }
  return second;
}

const newFunc = first();
// const newFunc = const second = () => {
// alert(greet);
// }
newFunc();

// CURRYING
const multiply = (a, b) => a * b;
const curriedMultiply = (a) => (b) => a * b;
// curriedMultiply(6)(3); // outputs 18
const multiplyBy7 = curriedMultiply(7);
// multiplyBy7(4); // outputs 28;

// COMPOSE
// Putting two funcs together, where input of one is the output of the other
const compose = (f, g) => (a) => f(g(a));
// Chaining functions together so that the output is the last 'function'
// (Fluffykins) is a (tiny) dragon that breathes (lightning). (Thanks mpj!).

// Avoiding side effects, functional purity.
// Side effects are things that affect things outside that function's 'universe', e.g. console.log or changing a global variable.
// Determinism: a function should always return exactly the same output for the same inputs.
