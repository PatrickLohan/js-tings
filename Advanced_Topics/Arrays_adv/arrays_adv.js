// Advanced arrays

const array =[1, 2, 5, 16, 23];

// Initialize empty array for pushing onto
const double =[];

// iterate over array (map?) and call 'double' for result
const newArray = array.forEach((num) => {
  double.push(num*2);
})

// We can use new map method
const mapDouble = array.map(num => {
  return num*2;
})
// because single arg, we can refactor to single line:
const mapDoubleClean = array.map(num => num * 2);

console.log('map', mapDouble, mapDoubleClean)

// filter
const filterArray = array.filter(num => num > 5);
console.log('filter', filterArray);

// reduce - accumulates array?
const reduceArray = array.reduce((accumulator, num) => {
  return accumulator + num
}, 5);

console.log('reduce', reduceArray);
