////////////////////////////////////////////////////////////////
// reference type///////////////////////////////////////////////
////////////////////////////////////////////////////////////////

const object1 = { value: 10 };
const object2 = object1;
const object3 = { value: 10};
//object1 === object2 // true
//object1 === object3 // false
//
// objects are reference types. they are defined by the human,and
// the computer will not know the similarities between two objects.



////////////////////////////////////////////////////////////////
// context vs scope/////////////////////////////////////////////
////////////////////////////////////////////////////////////////

function(b) {
  let a = 4;
}
console.log(a);
// will return reference error because outside scope
console.log(this);
// returns the window object
const object4 = {
  a: function() {
    console.log(this);
  }
}
// returns the function instead of the scope above (window)



////////////////////////////////////////////////////////////////
//instantiation/////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

// multiple instances of one object
class Player {
  constructor(name, type) {
    this.name = name;
    this.type = type;
  }
  introduce() {
    console.log(`Hi, I am ${this.name}. I'm a ${this.type}`)
  }
}

class Wizard extends Player {
  constructor(name, type) {
    super(name, type)
  }
  play() {
    console.log(`Weeeee! I'm a ${this.type}`)
  }
}

const wizard1 = new Wizard('Shelly', 'Healer');
const wizard2 = new Wizard('Sean', 'Pyromancer');
